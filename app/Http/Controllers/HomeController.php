<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getMessage(Request $request)
    {
        try {
            $auth_token = $request->input('auth_token');
            $id = $request->input('id');
            $message = $request->input('message');

            $client = new Client();
            $res = $client->request('POST', env('SOCKET_URL') . '/api/details', [
                'form_params' => [
                    'user_id' => $id,
                    'message' => $message
                ],
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . $auth_token
                ],
            ]);
            $response = $res->getBody();

            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
