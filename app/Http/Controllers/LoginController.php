<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        try {
            $email = $request->input('email');
            $password = $request->input('password');

            $client = new Client(['allow_redirects' => true]);
            $request = $client->request('POST', env('SOCKET_URL').'/api/login', [
                'form_params' => [
                    'email' => $email,
                    'password' => $password
                ]
            ]);
            $data['_token'] = json_decode((string)$request->getBody(), true)['token'];

            return view('home', $data);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
