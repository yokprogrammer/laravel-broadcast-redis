@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ url('getMessage') }}">
        @csrf
        <input type="text" name="auth_token" id="auth_token" value="{{ $_token }}">
        <input type="text" name="id" id="userId" value="" placeholder="user id">
        <input type="text" name="message" id="message" value="" placeholder="message">
        <input type="submit" id="btn" value="Send">
    </form>
@endsection
